package net.iescierva.mim.p0401automata;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;

public class Automata {
    Method evalInput;   //método que devuelve un elemento de VALID_INPUTS
    Method action;      //método que procesa las acciones del autómata
    List<VALID_STATES> finalStates;   //Lista de estados aceptables como estados finales
    VALID_STATES[][] transitions;     //Tabla de transiciones

    VALID_STATES currentState;        //estado actual del autómata
    VALID_STATES initialState;        //estado inicial del autómata (para conservarlo)
    Object o;                         //objeto sobre el que se procesan las acciones

    public Automata(
                    Method evalInput,
                    Method action,
                    Object o,
                    VALID_STATES [][] transitions,
                    VALID_STATES initialState,
                    List <VALID_STATES> finalStates
                    ) {
        this.evalInput=evalInput;
        this.action=action;
        this.o=o;
        this.transitions=transitions;
        this.initialState =initialState;
        this.finalStates =finalStates;
    }

    public boolean isValid(String s) {
        char c;
        VALID_STATES previousState;   //guardaremos el estado antes de hacer transición
        currentState = initialState;  //colocar en estado inicial
        VALID_INPUTS input=null;      //input contendrá el elemento de VALID_INPUTS detectado en c

        //para cada letra
        for (int i = 0; i < s.length(); i++) {
            c = s.charAt(i);

            try {   // EVALUAR ENTRADA: ver de qué tipo de entrada se trata
                input=(VALID_INPUTS)evalInput.invoke(null,c);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            }

            previousState= currentState;  //conservar estado anterior
            //calcular siguiente estado
            currentState =transitions[currentState.ordinal()][input.ordinal()];

            if (currentState ==null)
                return false;   //ERROR: no estaba registrado en la tabla de transiciones
            else{  // EJECUTAR ACCIÓN
                try {
                    action.invoke(o,previousState, currentState,c);
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                } catch (InvocationTargetException e) {
                    e.printStackTrace();
                }
            }
        }
        //si el último estado actual es final, el número es correcto
        return finalStates.contains(currentState);
    }
}