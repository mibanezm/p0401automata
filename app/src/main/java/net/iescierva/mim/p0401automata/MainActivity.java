package net.iescierva.mim.p0401automata;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import static net.iescierva.mim.p0401automata.VALID_INPUTS.*;
import static net.iescierva.mim.p0401automata.VALID_STATES.*;


//Enumeración de los Estados válidos posibles (las bolas del autómata)
enum VALID_STATES {
    ERROR,
    MANTISSA_SIGN,
    MANTISSA_INTEGER,
    MANTISSA_DECIMAL,
    EXPONENT_SIGN,
    EXPONENT
}

//Enumeración de los diferentes tipos de entrada posibles (los arcos del autómata)
enum VALID_INPUTS {
    ERROR,
    SIGN,  // +, -
    DOT,
    DIGIT, //0..9
    EXPONENTIAL  //e E
}

public class MainActivity extends AppCompatActivity {

    //Vistas del layout
    private EditText validationString;
    private TextView btnValidate,textExplored;

    //Variables para definir el autómata
    private Automata automata;
    private VALID_STATES[][] transitions;
    private List<VALID_STATES> finalStates;

    private DoubleNumber n;

    //Clase para ir calculando el número explorado por el autómata
    class DoubleNumber {
        int mantissaSign, exponentSign, numDecimals;
        double mantissaValue, exponentValue;

        public DoubleNumber() {
            delete();
        }

        public void delete() {
            mantissaValue = exponentValue =0D;
            mantissaSign = exponentSign =1;  //inicialmente es positivo
            numDecimals =0;

        }

        public double value() {
            return mantissaSign * mantissaValue *Math.pow(10, exponentSign * exponentValue);
        }

        public String toString() {
            return Double.toString(value());
        }

        //método que realiza la acción de calcular el número conforme el autómata avanza
        public double action(VALID_STATES previous,
                             VALID_STATES current,
                             char c) {
            if (current==MANTISSA_SIGN) {
                if (c=='-')
                    mantissaSign *=-1;
            } else if (current==MANTISSA_INTEGER)
                mantissaValue = mantissaValue *10+(c-'0');
            else if (current==MANTISSA_DECIMAL && c!='.')
            {
                numDecimals++;
                mantissaValue = mantissaValue +(c-'0')/Math.pow(10, numDecimals);
            } else if (current==EXPONENT_SIGN) {
                if (c=='-')
                    exponentSign *=-1;
            } else if (current==EXPONENT)
                exponentValue = exponentValue *10+(c-'0');
            return value();
        }
    }

    //método que dado un caracter de entrada nos dice qué tipo de entrada es
    public static VALID_INPUTS evalInput(char c) {
        if (c>='0' && c<='9')
            return DIGIT;
        else if (c=='-' || c=='+')
            return SIGN;
        else if (c=='.')
            return DOT;
        else if (c=='e' || c=='E')
            return EXPONENTIAL;
        else
            return VALID_INPUTS.ERROR;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init_automata();
    }

    //método que define completamente el autómata y llama al constructor
    private void init_automata() {
        validationString = findViewById(R.id.cadena);
        btnValidate = findViewById(R.id.btnValidar);
        textExplored = findViewById(R.id.textExplored);

        n=new DoubleNumber();

        transitions =new VALID_STATES[VALID_STATES.values().length][VALID_INPUTS.values().length];

        finalStates= new ArrayList<VALID_STATES>();
        finalStates.add(MANTISSA_INTEGER);
        finalStates.add(MANTISSA_DECIMAL);
        finalStates.add(EXPONENT);

        transitions[MANTISSA_SIGN.ordinal()][SIGN.ordinal()]= MANTISSA_SIGN;
        transitions[MANTISSA_SIGN.ordinal()][DOT.ordinal()]=MANTISSA_DECIMAL;
        transitions[MANTISSA_SIGN.ordinal()][DIGIT.ordinal()]= MANTISSA_INTEGER;

        transitions[MANTISSA_INTEGER.ordinal()][DOT.ordinal()]=MANTISSA_DECIMAL;
        transitions[MANTISSA_INTEGER.ordinal()][DIGIT.ordinal()]=MANTISSA_INTEGER;
        transitions[MANTISSA_INTEGER.ordinal()][EXPONENTIAL.ordinal()]=EXPONENT_SIGN;

        transitions[MANTISSA_DECIMAL.ordinal()][DIGIT.ordinal()]=MANTISSA_DECIMAL;
        transitions[MANTISSA_DECIMAL.ordinal()][EXPONENTIAL.ordinal()]=EXPONENT_SIGN;

        transitions[EXPONENT_SIGN.ordinal()][SIGN.ordinal()]=EXPONENT_SIGN;
        transitions[EXPONENT_SIGN.ordinal()][DIGIT.ordinal()]=EXPONENT;

        transitions[EXPONENT.ordinal()][DIGIT.ordinal()]=EXPONENT;

        Method m= null, a=null;
        try {
            m = MainActivity.class.getDeclaredMethod("evalInput", char.class);
            a = DoubleNumber.class.getMethod("action",VALID_STATES.class, VALID_STATES.class, char.class);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
        automata=new Automata(m,a,n,transitions,MANTISSA_SIGN,finalStates);
    }

    //método que se invoca al pulsar el botón de validación
    public void validate(View view) {
        String num = validationString.getText().toString();
        n.delete();  //borramos el valor numérico antes de llamar al autómata

        if (automata.isValid(num)) {
            //si la validación es correcta
            btnValidate.setText(R.string.validFormat);
            textExplored.setText(n.toString());
        }
        else {
            //si falló la validación
            btnValidate.setText(R.string.invalidFormat);
            textExplored.setText("");
        }
    }
}
